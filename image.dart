import 'package:flutter/material.dart';

class ImageWidget extends StatefulWidget {
  const ImageWidget({super.key});

  @override
  State<ImageWidget> createState() => _ImageWidgetState();
}

class _ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Image Widget'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CircleAvatar(
              radius: 60,
              backgroundColor: Colors.blue,
              backgroundImage: AssetImage('assets/image/image1.jpeg'),
              // backgroundImage: NetworkImage(
              //     'https://images.unsplash.com/photo-1544005313-94ddf0286df2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=776&q=80'), //For Network
            ),
            //---------------------------------------------------------//
            const SizedBox(height: 20),
            //---------------------------------------------------------//
            ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: const Image(
                  height: 100,
                  //image: AssetImage('assets/image/image2.jpeg'),
                  image: NetworkImage(
                      'https://images.unsplash.com/photo-1554151228-14d9def656e4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=772&q=80'),
                )),
            //---------------------------------------------------------//
            const SizedBox(height: 20),
            //---------------------------------------------------------//
            //Image local
            Image.asset(
              'assets/image/girlred.jpeg',
              height: 200,
              width: 200,
            ),
            //---------------------------------------------------------//
            const SizedBox(height: 20),
            //---------------------------------------------------------//
            //Image network
            Image.network(
                height: 200,
                width: 200,
                'https://53.fs1.hubspotusercontent-na1.net/hub/53/hubfs/image8-2.jpg?width=1190&height=800&name=image8-2.jpg')
          ],
        ),
      ),
    );
  }
}
